#!/usr/bin/env bash

docker run \
  -v "$(pwd)"/../:/c:rw \
  --network=testcontainers_ext \
  --ip=172.28.5.2 \
  -e SSH_PRIVATE_KEY="$(cat ../testcontainers/sshkeys/admin)" \
  -e ANSIBLE_VAULT_PASSWORD_FILE=/c/private/ansible-password \
  -e DIR=/c/float-minimal \
  float-runner:latest \
  run $1
