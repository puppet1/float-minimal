#!/usr/bin/env bash
# ssh's into container with float tools + testsshkey 

#if [[ "$(docker images -q float-runner:latest 2> /dev/null)" == "" ]]; then
  docker build -f ../testcontainers/float-runner.Dockerfile -t float-runner ../testcontainers
#fi
docker run \
  -v "$(pwd)"/../:/c:rw \
  --network=testcontainers_ext \
  --ip=172.28.5.2 \
  -e SSH_PRIVATE_KEY="$(cat ../testcontainers/sshkeys/admin)" \
  -e ANSIBLE_VAULT_PASSWORD_FILE=/c/private/ansible-password \
  -u $(id -u ${USER}):$(id -g ${USER}) \
  -e DIR=/c/float-minimal \
  -it \
  float-runner:latest
